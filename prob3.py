# The prime factors of 13195 are 5, 7, 13 and 29.
# 
# What is the largest prime factor of the number 600851475143 ?

import math

number = 600851475143
# number = 100
i = 2
factors = []

while number > 1:
    while (number % i == 0):
        factors.append(i)
        number /= i
    i += 1


print factors
