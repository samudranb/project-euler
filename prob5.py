""" 
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20
"""

factors = [2]
smallest = 2

for number in range(3,21):
    for prior_factor in factors:
        if (number % prior_factor == 0):
            number /= prior_factor
    factors.append(number)
    smallest *= number
    
print smallest
