"""
Iterate over the whole number system, ignoring even numbers greater than 2 (2, 3, 5, 7,)
For each integer, p, check if p is prime
Iterate over the primes already found which are less than the square-root of p
For each prime in this set, f, check to see if it is a factor of p:
If f divides p then p is non-prime. Continue from 2 for the next p.
If no factors are found, p is prime. Continue to 3.
If p is not the nth prime we have found, add it to the list of primes. Continue from 2 for the next p.
Otherwise, p is the nth prime we have found and we should return it.
"""

import math

def is_prime(number):
    primes = [2]
    
    possible_factors = range(3, int(math.sqrt(number)+1), 2)
    print "possible:",possible_factors
    for possible_factor in possible_factors:
        print "possible factor:", possible_factor
        if is_prime(possible_factor):
            primes.append(possible_factor)
            
    print primes
    
print is_prime(9)
