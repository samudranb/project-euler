"""
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.
"""

import prime_sieve

def add(x,y):
    return x + y

print reduce(add, prime_sieve.list_of_primes_less_than_number(2000000) )
# print prime_sieve.list_of_primes_less_than_number(2000000)
