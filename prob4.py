"""
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 x 99.

Find the largest palindrome made from the product of two 3-digit num"""

number = 98901

def is_palindrome(number):
    palindrome = True
    split_number = []
    
    num_of_digits = len(str(number))

    while (number > 9):
        split_number.append( number % 10 )
        number /= 10
    split_number.append(number) # last digit
    
    i = 0
    while (i < num_of_digits):
        if (split_number[i] != split_number[num_of_digits - i - 1]):
            palindrome = False
        i += 1        
    
    return palindrome

a, b = 999, 999
palindrome_list = []

while (a > 99 ):
    b = 999
    while ( b > 99 ):
        if (is_palindrome(a*b)) :
            palindrome_list.append(a*b)
        b -= 1
    a -= 1

print max(palindrome_list)
