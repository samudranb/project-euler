"""
To check if the inputted number is a prime number, using sieve of Eratosthenes

Implements two functions: 
- list_of_primes_less_than_number(number)
- is_prime(number)
"""

def list_of_primes_less_than_number(number):
    """
    Returns a list of all primes less than the number. Uses Sieve of Eratosthenes
    """
    # Initialization
    potentials = range(2, number+1)
    
    j = 0
    
    while j < number-1 : 
        if potentials[j] != -1 :
            i = potentials[j] + j # skip the first number - hence "+j"
            while i < number-1 : 
                potentials[i] = -1
                i += potentials[j]
        
        j += 1
        
    primes = [ x for x in potentials if x != -1 ]
    return primes

def is_prime(number):
    """
    Checks if the number given is a prime, uses sieve of eratosthenes
    """
    primes = list_of_primes_less_than_number(number)
    #print primes
    if primes[-1] == number:
        return True
    return False

def main():
    print is_prime(3457)
    
if __name__ == "__main__":
    main()
