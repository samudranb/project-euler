"""The sum of the squares of the first ten natural numbers is,

1^2 + 2^2 + ... + 10^2 = 385
The square of the sum of the first ten natural numbers is,

(1 + 2 + ... + 10)^2 = 55^2 = 3025
Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 - 385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
"""

def sum_of_squares(number):
    result = 0
    i = 1
    while i <= number:
        result += i ** 2
        i += 1
    return result
        
def square_of_sum(number):
    result = 0
    i = 1
    while i<= number:
        result += i
        i += 1        
    return result ** 2
    
print abs(sum_of_squares(100) - square_of_sum(100))
